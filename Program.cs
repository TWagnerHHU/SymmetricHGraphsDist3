﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        //Sucht alle Symmetrischen Dist 2 Hypergraphencodes die zusätzlich die notwendige Dist 3 Bedingung erfüllen auf bis zu searchLength Qubits
        static void Main(string[] args)
        {
            bool resultfound = false;
            int searchLength = 30;
            List<string> linesL = new List<string>(); //für textfile verarbeitung            
            List<indizes>[][] results = new List<indizes>[searchLength][];  //Resultate werden hier gespeichert, erster index ist Zeile, zweiter ist (l = Anzahl Z Gates - 1)

            for (int i = 0; i < searchLength; i++)  // Iteriere über Anzahl Qubits des X-Differenz Hypergraphen D   
            {

                ulong[] binoms = fillBinom(i); //i´te Zeile des Pascalschen Dreiecks
                ulong t = (ulong)Math.Pow(2, i - 1);
                ulong u = (ulong)Math.Pow(2, i - 2);

                indizes[] possibilities = AllPossibleBinomSums(binoms, t);  //Es werden nur ergebnisse genommen wo der erste index nicht 0 ist, da der nullindex nur einem globalem minuszeichen entspricht

                results[i] = new List<indizes>[i];

                for (int l = 0; l < i; l++) 
                {
                    Console.WriteLine("i = " + i + " l = " + l);
                    results[i][l] = new List<indizes>();
                
                    ulong[] binomsumsl = fillBinomSums(i, l); // Summen über Binomialkoeffizienten wie sie im Kriterium benutzt werden
                    ulong[] binomsumsHigherl = fillBinomSums(i, l + 1);               
                 
                  //  Console.WriteLine("Zeile: " + i + " Zielzahl " + t +" Z Gates: " + (l+1)); //show where the search is
                             

                    //Implementierung meines Kriteriums:
                    //indizes[] possibilities = AllPossibleSums(binomsumsl, u);                    
                    for (int j = 0; j < possibilities.Length; j++)
                    {                       
                        
                        if(CheckSum(binomsumsl,u,possibilities[j]))
                        {
                            if (CheckSum(binomsumsHigherl, u, possibilities[j]))
                            {
                                if (!possibilities[j].corespondsToGraphState(i)) // show only results that do not correspond to graph states
                                {

                                    results[i][l].Add(possibilities[j]);
                                }
                            }
                        }
                    }

                }

            }


            //Hier wird nun die notwendige bedingung für dist 3 geprüft

            for (int i = 0; i < results.Length; i++)
            {
                for (int l = 1; l < results[i].Length - 1; l++)
                {
                    foreach (indizes ind in results[i][l])
                    {
                        if (ind.isContainedIn(results[i][l + 1]))
                        {
                            if (ind.isContainedIn(results[i][l - 1]))
                            {
                                linesL.Add("Result: " + " Qubits: " + i + " Z Gates: " + (l + 1) + " Indizes: " + ind.IndizesAsString());
                                resultfound = true;
                            }
                        }
                    }
                }
            }

            Console.WriteLine(resultfound);
            string[] lines = linesL.ToArray();
            System.IO.File.WriteAllLines(@"C:\Users\User\Documents\Uni\QInfo\Bachelorarbeit\SymmetrischeHGraphDist3\SymmetrischeHypergraphenPascalDreieck\output\result.txt", lines);
            

            Console.ReadLine();






            

         /*   while (true)
            {
                int n = int.Parse(Console.ReadLine());
                int k = int.Parse(Console.ReadLine());
                Console.WriteLine(binom(n, k));
            }
          */   

         
        }


        public static bool CheckSum(ulong[] n, ulong t, indizes ind) // Ist die Summe über alle i in indizes n[i] gleich t?
        {
            ulong r = 0;
            int[] indizes = ind.getIndizes();
            for (int i = 0; i < indizes.Length; i++)
            {
                r = r + n[indizes[i]];
            }
            if (r == t)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



       //wie allPossibleSums aber keine ergebnisse wo der erste index 0 ist
        public static indizes[] AllPossibleBinomSums(ulong[] n, ulong t) 
        {
            List<indizes> l = allPossibleSums(n, t, 0);

            l.RemoveAll(delegate(indizes i) { return (i.getIndizes()[0] == 0); }); //beachte, dass die indiztes sortiert sind

            indizes[] r = l.ToArray();
            return r;
        }

        // gibt ein leeres array zurück wenn es keine Möglichkeit gibt
        public static indizes[] AllPossibleSums(ulong[] n, ulong t) // gibt alle Möglichkeiten zurückt, t als summe von zahlen aus n darzustellen, wobei n nur strikt positive Zahlen enthält. Die Rückgabe sind alle Möglichen Kombinationen von Indizes in n, die dies erfüllen
        {
            List<indizes> l = allPossibleSums(n, t, 0);
            
            indizes[] r = l.ToArray();
            return r; 
        }



        // Hilfsmethode für AllPossibleSums
        public static List<indizes> allPossibleSums(ulong[] n, ulong t, int shift) // gibt alle Möglichkeiten zurückt, t als summe von zahlen aus n darzustellen, wobei n nur strikt positive Zahlen enthält. Die Rückgabe sind alle Möglichen Kombinationen von Indizes in n, die dies erfüllen
        {
            List<indizes> l = new List<indizes>();
            if (n.Length == 0) // Abbruchbedingung
            {
                return l;
            }
            
           
            for (int i = 0; i < n.Length; i++)
            {
                if (n[i] == t) //EIne Zahl ist schon die Zielzahl
                {
                    // Console.WriteLine(n[i] + " ");
                    indizes ind = new indizes();
                    ind.addIndex(i + shift);
                    l.Add(ind);                   
                }

                if (n[i] < t) // Nur in diesem Fall  kann man evtl. weitere Zahlen addieren um die Zielzahl zu erhalten => rekursiver Aufruf (Die Liste erhält keine negativen Zahlen)
                {
                    ulong[] r = new ulong[n.Length - i - 1]; // r wird n ohne die ersten i+1 zahlen
                    for (int j = 0; j < r.Length; j++)
                    {
                        r[j] = n[j + i + 1];
                    }

                    List<indizes> b = allPossibleSums(r, t - n[i], i + shift + 1); //Neue Zielzahl ist t - n[i], da n[i] in der Summe benutzt wird

                    if (b != null)
                    {
                        /*foreach (indizes j in b)
                        {
                            j.addIndex(i + shift);
                            l.Add(j);
                        }
                         */
                        for (int j = 0; j < b.Count; j++)
                        {
                            b[j].addIndex(i + shift);                           
                            l.Add(b[j]);
                        }
                    }
                    
                }

            }            
            
            return l;
            
            
        }
       
         



        public static ulong[] fillBinomSums(int n, int l) // um das allgemeinere Kriterium zu prüfen, wo nur l Z-Gates angewandt werden. Es muss gelten: l<=n
        {
           
            ulong[] r = new ulong[n + 1];
            ulong temp = 0;
            for (int i = 0; i < r.Length; i++)
            {
                temp = 0;
                for (int j = 1; j < r.Length; j = j + 2)  
                {
                    
                    temp += binom(l, j) * binom(n - l, i - j);
                }
                r[i] = temp;
            }
           
            return r;
        }





        public static ulong[] fillBinom(int n) // n´te Zeile des Pascalschen Dreiecks 
        {
            ulong[] r = new ulong[n + 1];
            for (int i = 0; i < r.Length; i++)
            {
                r[i] = binom(n, i);
            }
           
            return r;
        }       

    

        public static ulong binom(int n, int k) // von C# funktioniert für große Zahlen, mindestens bis n = 50
        {
            
            if (n < k || k < 0)
            {
                return 0;
            }
            ulong result = 1;
            for (ulong i = 1; i <= (ulong)k; i++)
            {
                result *= (ulong)n - ((ulong)k - i);
                result /= i;
            }
            
            return result;
        }
        

    }


    class indizes
    {
        private int[] ind;

        public indizes() 
        {
            
        }

        public bool isContainedIn(List<indizes> l)
        {
            foreach(indizes indiz in l)
            {
                if (isEqualTo(indiz))
                {
                    return true;
                }
            }
                return false;
        }

        public bool isEqualTo(indizes compare)
        {
            int[] temp = compare.getIndizes();
            if (temp.Length != ind.Length)
            {
                return false;
            }
            for (int i = 0; i < ind.Length; i++)    //beachte: indizes sidn sortiert
            {
                if (ind[i] != temp[i])
                {
                    return false;
                }
            }
            return true;
        }

        public void addIndex(int a)
        {
            if (ind != null)
            {
                int[] n = new int[ind.Length + 1];
                for (int i = 0; i < ind.Length; i++)
                {
                    n[i] = ind[i];
                }
                n[ind.Length] = a;
                ind = n;
            }
            else
            {
                ind = new int[1];
                ind[0] = a;
            }
            Array.Sort(ind);
        }

        public int[] getIndizes()
        {
            return ind;
        }

        public void printindizes()
        {
            for (int i = 0; i < ind.Length; i++)
            {
                Console.Write(" " + ind[i]);
            }
            Console.WriteLine();
        }

        public string IndizesAsString()
        {
            string s = "";
            for (int i = 0; i < ind.Length; i++)
            {
                s = s + " " + ind[i].ToString(); ;
            }
            return s;
        }

        public bool corespondsToGraphState(int n) //n = number of qubits, es wird geprüft ob alle ungeraden indizes vorhanden sind
        {


            int l = (int)((n + 1) / 2);
            if(ind.Length<l) //enthält nicht alle indizes
            {
                return false;
            }

            for(int i = 0; i< ind.Length;i++) // ind ist sortiert
            {
                if(ind[i] != 2*i+1)
                {
                    return false;
                }                
            }            
           
            return true;
        }       
    }
}
